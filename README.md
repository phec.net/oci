# Construction de l'environnement de base

Lancer une construction sur notre binder connecté à Google Compute Registry : /v2/gl/phec.net/oci/platform-base
Récupérer l'image créée que nous appellerons BASE (docker pull gcr.......)
Retagguer l'image (docker tag gcr....... registry.gitlab.com/phec.net/oci/platformbase:base)
